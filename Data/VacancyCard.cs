﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Data
{
    class VacancyCard
    {
        public string Title { get; set; }
        public string Department { get; set; }
        public string Desctiption { get; set; }
        public string Address { get; set; }
        public string Language { get; set; }

    }
}
