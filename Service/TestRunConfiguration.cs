﻿using SeleniumTask.Service.Driver;
using System;

namespace SeleniumTask.Service
{
    class TestRunConfiguration
    {

        public static Browser GetBrowser()
        {
            return (Browser)Enum.Parse(typeof(Browser), Properties.Resources.Browser);
        }

        public static string GetBaseUrl()
        {
            return Properties.Resources.Url;
        }

    }
}
