﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace SeleniumTask.Service.Driver
{
    class DriverHelper
    {

        public static IWebDriver GetDriver(Browser requiredBrowser)
        {
            IWebDriver driver = requiredBrowser switch
            {
                Browser.Firefox => new FirefoxDriver(),
                Browser.Chrome => new ChromeDriver(),
                _ => throw new ArgumentOutOfRangeException(),
            };
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            return driver;
        }

        public static IWebDriver GetSpecifiedDriver()
        {
            return GetDriver(TestRunConfiguration.GetBrowser());
        }

        public static void СloseDriver(IWebDriver driver)
        {
            driver.Close();
            driver.Quit();
        }

    }
}
