﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Service.Driver
{
    class CustomConditions
    {

        public static Func<IWebDriver, bool> ElementIsHidden(By locator)
        {
            Func<IWebDriver, bool> isHidden = driver =>
            {
                var classes = driver.FindElement(locator).GetAttribute("class");
                return classes.Contains("hide");
                
            };
            return isHidden;
        }

    }
}
