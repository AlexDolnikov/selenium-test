﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Elements
{
    class MultiselectElement : BaseElement<MultiselectElement>
    {

        public MultiselectElement(IWebElement element) : base(element)
        {
        }

        public MultiselectElement(By locator, IWebDriver driver) : base(locator, driver)
        {
        }

        public void SelectById(string id)
        {
            Click();
            FindThisElement().FindElement(GetItemSelectorById(id)).Click();
        }

        protected override MultiselectElement GetPresetedElement(IWebElement element)
        {
            throw new NotImplementedException();
        }

        private By GetItemSelectorById(string id)
        {
            return By.XPath($".//label[@for='{id}']");
        }
    }
}
