﻿using OpenQA.Selenium;
using System;

namespace SeleniumTask.Elements
{
    class SelecterElement : BaseElement<SelecterElement>
    {

        public SelecterElement(IWebElement element) : base(element)
        {
        }

        public SelecterElement(By locator, IWebDriver driver) : base(locator, driver) 
        {
        }

        public void SelectByText(string text)
        {
            Click();
            FindThisElement().FindElement(getItemSelectorByText(text)).Click();
        }

        protected override SelecterElement GetPresetedElement(IWebElement element)
        {
            throw new NotImplementedException();
        }

        private By getItemSelectorByText(string text)
        {
            return By.XPath($".//span[contains(@class, 'selecter-item') and text() = '{text}']");
        }


    }
}
