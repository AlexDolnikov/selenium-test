﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTask.Service.Driver;
using System;
using System.Collections.Generic;

namespace SeleniumTask.Elements
{
    abstract class BaseElement<T>
    {

        protected IWebElement PresetElement { get; set; }
        protected IWebDriver Driver { get; set; }
        protected By Locator { get; set; }

        protected BaseElement(IWebElement element)
        {
            PresetElement = element;
        }

        protected BaseElement(By locator, IWebDriver driver)
        {
            Driver = driver;
            Locator = locator;
        }

        public IWebElement FindThisElement()
        {
            return PresetElement ?? Driver.FindElement(Locator);
        }

        public List<T> FindAll()
        {
            var result = new List<T>();
            var elements = Driver.FindElements(Locator);
            foreach (var element in elements)
            {
                result.Add(GetPresetedElement(element));
            }
            return result;
        }

        protected abstract T GetPresetedElement(IWebElement element);

        public void Click()
        {
            FindThisElement().Click();
        }

        public bool IsEnabled()
        {
            return FindThisElement().Enabled;
        }

        public void WaitUntilHidden()
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(CustomConditions.ElementIsHidden(Locator));
        }
    }
}
