﻿using OpenQA.Selenium;
using SeleniumTask.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Elements
{
    class VacancyCardElement : BaseElement<VacancyCardElement>
    {

        private readonly By TitleLocator = By.XPath(".//a[contains(@class, 'vacancies-blocks-item-header')]");
        private readonly By DepartmentLocator = By.XPath(".//p[@class='caption']");
        private readonly By DescriptionLocator = By.XPath(".//div[contains(@class, 'vacancies-blocks-item-description-text')]");
        private readonly By AddressLocator = By.XPath(".//span[@itemprop='address']");
        private readonly By LanguageLocator = By.XPath(".//p[@class='languages m0']");

        public VacancyCardElement(IWebElement element) : base(element)
        {
        }

        public VacancyCardElement(By locator, IWebDriver driver) : base(locator, driver)
        {
        }

        public VacancyCard GetData()
        {
            return new VacancyCard {
                Title = GetTitleElement().Text,
                Department = GetDepartmentElement().Text,
                Desctiption = GetDescriptionElement().Text,
                Address = GetAddressElement().Text,
                Language = GetLanguageElement().Text
            };
        }

        public IWebElement GetTitleElement()
        {
            return FindThisElement().FindElement(TitleLocator);
        }

        public IWebElement GetDepartmentElement()
        {
            return FindThisElement().FindElement(DepartmentLocator);
        }

        public IWebElement GetDescriptionElement()
        {
            return FindThisElement().FindElement(DescriptionLocator);
        }

        public IWebElement GetAddressElement()
        {
            return FindThisElement().FindElement(AddressLocator);
        }

        public IWebElement GetLanguageElement()
        {
            return FindThisElement().FindElement(LanguageLocator);
        }

        protected override VacancyCardElement GetPresetedElement(IWebElement element)
        {
            return new VacancyCardElement(element);
        }
    }
}
