﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Elements
{
    class ButtonElement : BaseElement<ButtonElement>
    {

        public ButtonElement(IWebElement element) : base(element)
        {
        }

        public ButtonElement(By locator, IWebDriver driver) : base(locator, driver)
        {
        }

        protected override ButtonElement GetPresetedElement(IWebElement element)
        {
            return new ButtonElement(element);
        }
    }
}
