using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumTask.Page;
using SeleniumTask.Service.Driver;

namespace SeleniumTask.Tests
{
    public class VeeamTest
    {

        private IWebDriver Driver;
        private CareersPage Careers;

        [SetUp]
        public void Setup()
        {
            Driver = DriverHelper.GetSpecifiedDriver();
            Careers = new CareersPage(Driver);
        }

        [TestCase("Romania", "ch-7", 20)]
        public void Test1(string country, string languageId, int count)
        {
            // when
            Careers.GoToPage();
            Careers.CounrySelect.SelectByText(country);
            Careers.LanguageSelect.SelectById(languageId);
            Careers.ShowAllJobsButton.Click();
            Careers.ShowAllJobsButton.WaitUntilHidden();
            var vacancyCards = Careers.VacancyCard.FindAll();
            // then
            Assert.AreEqual(count, vacancyCards.Count);
        }

        [TearDown]
        public void TearDown()
        {
            DriverHelper.—loseDriver(Driver);
        }
    }
}