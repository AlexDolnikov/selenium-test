﻿using OpenQA.Selenium;
using SeleniumTask.Elements;
using SeleniumTask.Service;
using SeleniumTask.Service.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumTask.Page
{
    class CareersPage
    {

        private IWebDriver Driver { get; }

        public readonly SelecterElement CounrySelect;
        public readonly MultiselectElement LanguageSelect;
        public readonly ButtonElement ShowAllJobsButton;
        public readonly VacancyCardElement VacancyCard;

        public CareersPage(IWebDriver driver)
        {
            Driver = driver;
            CounrySelect = new SelecterElement(By.Id("country-element"), driver);
            LanguageSelect = new MultiselectElement(By.Id("language"), driver);
            ShowAllJobsButton = new ButtonElement(By.XPath("//a[contains(@class, 'load-more-button')]"), driver);
            VacancyCard = new VacancyCardElement(By.XPath("//div[contains(@class, 'vacancies-blocks-item ')]"), driver);
        }

        public void GoToPage()
        {
            Driver.Navigate().GoToUrl(TestRunConfiguration.GetBaseUrl());
        }

        
    }
}
